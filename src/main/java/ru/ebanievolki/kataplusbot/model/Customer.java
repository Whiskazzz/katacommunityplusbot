package ru.ebanievolki.kataplusbot.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "customers")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Customer {

    @Id
    Long id;

    @Column
    String name;

    @Column
    Long plusCount;

    @Column
    String tag;
}
